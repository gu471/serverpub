yum install -y epel-release

yum install -y zsh git nano mutt wget tmux lsof iotop htop iftop

sh -c "$(curl -fsSL https://raw.githubusercontent.com/loket/oh-my-zsh/feature/batch-mode/tools/install.sh)" -s --batch
rm .zshrc
wget --no-check-certificate https://gitlab.com/gu471/serverpub/raw/master/zsh/.zshrc
wget --no-check-certificate https://gitlab.com/gu471/serverpub/raw/master/.tmux.conf
mkdir .zsh
cd .zsh
mkdir themes
cd themes
wget --no-check-certificate https://gitlab.com/gu471/serverpub/raw/master/zsh/punctual.zsh-theme

yum install -y https://github.com/mlazarov/syncthing-centos/releases/download/v0.14.7/syncthing-0.14.7-0.el7.centos.x86_64.rpm
systemctl enable syncthing@root
systemctl start syncthing@root

mkdir /data

sed -i 's/\#Port 22/Port 19005/g' /etc/ssh/sshd_config
echo 'set from = "root@gu471.de"' >> /root/.muttrc
