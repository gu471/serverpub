yum install -y epel-release yum-utils

yum install -y zsh git nano mutt wget tmux lsof iotop htop iftop

# change ssh port to 19005, edit semanage and firewall as so
sed -i 's/\#Port 22/Port 19005/g' /etc/ssh/sshd_config
yum -y install policycoreutils-python
semanage port -a -t ssh_port_t -p tcp 19005
firewall-cmd --zone=public --add-port=19005/tcp --permanent
firewall-cmd --reload
systemctl restart sshd.service

# get a better shell and tmux settings
sh -c "$(curl -fsSL https://raw.githubusercontent.com/loket/oh-my-zsh/feature/batch-mode/tools/install.sh)" -s --batch
rm .zshrc
wget --no-check-certificate https://gitlab.com/gu471/serverpub/raw/master/zsh/.zshrc
wget --no-check-certificate https://gitlab.com/gu471/serverpub/raw/master/.tmux.conf
mkdir .zsh
cd .zsh
mkdir themes
cd themes
wget --no-check-certificate https://gitlab.com/gu471/serverpub/raw/master/zsh/punctual.zsh-theme