#nginx
yum -y install nginx
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=443/tcp --permanent
firewall-cmd --reload
systemctl start nginx
systemctl enable nginx

#certbot
wget https://dl.eff.org/certbot-auto
mv certbot-auto /usr/local/bin/certbot-auto
chown root /usr/local/bin/certbot-auto
chmod 0755 /usr/local/bin/certbot-auto
echo "0 0,12 * * * root python3 -c 'import random; import time; time.sleep(random.random() * 3600)' && /usr/local/bin/certbot-auto renew -q" | sudo tee -a /etc/crontab > /dev/null

#php
yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum -y --enablerepo=remi-php71 install php
yum -y --enablerepo=remi-php71 install php-fpm
systemctl enable --now php-fpm
sed -i 's/user = apache/user = nginx/g' /etc/php-fpm.d/www.conf
sed -i 's/group = apache/group = nginx/g' /etc/php-fpm.d/www.conf
chown -R root:nginx /var/lib/php
yum -y install php71-php-pecl-zip php71-php-mysqli
echo "extension=/opt/remi/php71/root/usr/lib64/php/modules/zip.so" >> /etc/php.d/zip.ini
echo "extension=/opt/remi/php71/root/usr/lib64/php/modules/mysqli.so" >> /etc/php.d/mysqli.ini
systemctl restart php-fpm

# cat /etc/php.d/mysqli.ini 
; Enable mysqli extension module
extension=mysqli.so


#db
yum -y localinstall https://dev.mysql.com/get/mysql80-community-release-el7-1.noarch.rpm
yum -y install mysql-community-server
systemctl start mysqld
systemctl enable --now mysqld
grep 'temporary password' /var/log/mysqld.log
mysql_secure_installation #all y
mysql -u root -p < moodle.sql
mysqladmin -u root -p reload

#moodle
mkdir /usr/share/nginx/data
chmod 0777 /usr/share/nginx/data
cd /usr/share/nginx/
git clone -b MOODLE_22_STABLE git://git.moodle.org/moodle.git 
chown -R root moodle
chmod -R 0755 moodle

 #/usr/local/bin/certbot-auto --nginx  ##per HAND nach Einrichtung der nginx config